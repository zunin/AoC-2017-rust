FROM rust:latest
LABEL maintainer="nikolai@oellegaard.com"
WORKDIR /code
ADD . .
CMD cargo build
