use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

pub fn solve_part_one(filename: &str) -> i32 {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);
    
    let mut sum = 0;
    
    for line in file.lines() {
        sum += parse_input(&line.unwrap());
    }
    return sum;
}

pub fn parse_input(input: &str) -> i32 {
    let mut sum = 0;
    let mut last_char = input.chars().last().unwrap();
    for curr_char in input.chars().into_iter() {
        if curr_char == last_char {
            sum += curr_char.to_digit(10).unwrap() as i32;
        }
        last_char = curr_char;
    }
    return sum;
}

pub fn solve_part_two(filename: &str) -> i32 {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let mut sum = 0;

    for line in file.lines() {
        sum += parse_input_two(&line.unwrap());
    }
    return sum;
}

pub fn parse_input_two(input: &str) -> i32 {
    let chars: Vec<i32> = input.chars()
        .map(|x|x.to_digit(10).unwrap() as i32)
        .collect();

    let sum = chars.iter()
        .enumerate()
        .map(|(index, x)|{
            let length = chars.len();
            (x, chars.get((index + (length / 2)) % length).unwrap())
        })
        .fold(0, |sum, (curr_val, next_val)| {
        if curr_val == next_val {
            return sum + curr_val;
        }
        return sum;
    });


    return sum;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_1_example_1() {
        assert_eq!(parse_input("1122"), 3);
    }

    #[test]
    fn part_1_example_2() {
        assert_eq!(parse_input("1111"), 4);
    }

    #[test]
    fn part_1_example_3() {
        assert_eq!(parse_input("1234"), 0);
    }

    #[test]
    fn part_1_example_4() {
        assert_eq!(parse_input("91212129"), 9);
    }

    #[test]
    fn part_one() {
        assert_eq!(solve_part_one("input.txt"), 1393);
    }

    #[test]
    fn part_2_example_1() {
        assert_eq!(parse_input_two("1212"), 6);
    }

    #[test]
    fn part_2_example_2() {
        assert_eq!(parse_input_two("1221"), 0);
    }

    #[test]
    fn part_2_example_3() {
        assert_eq!(parse_input_two("123425"), 4);
    }

    #[test]
    fn part_2_example_4() {
        assert_eq!(parse_input_two("123123"), 12);
    }

    #[test]
    fn part_2_example_5() {
        assert_eq!(parse_input_two("12131415"), 4);
    }

    #[test]
    fn part_two() {
        assert_eq!(solve_part_two("input.txt"), 1292);
    }
}
