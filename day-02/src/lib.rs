use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Error;
use std::cmp::min;
use std::cmp::max;

pub fn solve_part_one(filename: &str) -> i32 {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let spreadsheet = parse_input(file.lines().into_iter().collect());
    return get_checksum(spreadsheet);
}


pub fn solve_part_two(filename: &str) -> i32 {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let spreadsheet = parse_input(file.lines().into_iter().collect());
    return get_evenly_divisible_values(spreadsheet);
}

pub fn parse_input(iterator: Vec<Result<String, Error>>) -> Vec<Vec<String>> {
    return iterator.into_iter().map(|line| line.unwrap())
        .map(|line| {
            line.split("\t").map(|digit|digit.to_string()).collect()
        }).collect();
}

pub fn get_checksum(input: Vec<Vec<String>>) -> i32 {
    let mut sum = 0;

    for line in input {
        let mut parsed_input: Vec<i32> = line.into_iter().map(|digit|digit.parse::<i32>().unwrap()).collect();
        parsed_input.sort();
        sum += parsed_input.last().unwrap() - parsed_input.first().unwrap();
    }

    return sum;
}

pub fn get_evenly_divisible_values(input: Vec<Vec<String>>) -> i32 {
    let mut sum = 0;

    for line in input {
        let parsed_input: Vec<i32> = line.into_iter().map(|digit|digit.parse::<i32>().unwrap()).collect();
        'outer: for (index, digit) in parsed_input.iter().enumerate() {
            for inner_index in (index + 1)..(parsed_input.len()) {
                let inner_digit = parsed_input[inner_index];
                if (digit % inner_digit) == 0 || (inner_digit % digit) == 0 {
                    let largest = *max(digit, &inner_digit);
                    let smallest = *min(digit, &inner_digit);
                    let adder = largest / smallest;
                    sum += adder;
                    break 'outer;
                }
            }
        }
    }
    return sum;
}

#[cfg(test)]
mod tests {
    use super::*;

    fn example_input() -> Vec<Vec<String>> { vec![
        vec![String::from("5"), String::from("1"), String::from("9"), String::from("5")],
        vec![String::from("7"), String::from("5"), String::from("3")],
        vec![String::from("2"), String::from("4"), String::from("6"), String::from("8")]
    ]}

    #[test]
    fn example() {
        assert_eq!(get_checksum(example_input()), 18);
    }

    #[test]
    fn part_one_solution() {
        assert_eq!(solve_part_one("input.txt"), 44216);
    }

    fn example_2_input() -> Vec<Vec<String>> { vec![
        vec![String::from("5"), String::from("9"), String::from("2"), String::from("8")],
        vec![String::from("9"), String::from("4"), String::from("7"), String::from("3")],
        vec![String::from("3"), String::from("8"), String::from("6"), String::from("5")]
    ]}

    #[test]
    fn example_2() {
        assert_eq!(get_evenly_divisible_values(example_2_input()), 9)
    }

    #[test]
    fn part_two_solution() {
        assert_eq!(solve_part_two("input.txt"), 320);
    }
}
