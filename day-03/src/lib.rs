use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::collections::HashMap;

#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug)]
pub struct Point {
    x: i32,
    y: i32,
}

pub struct Spiral {
    x: i32,
    y: i32,
    coord_max: i32,
    x_rising: bool,
    y_rising: bool
}

impl Spiral {
    fn new() -> Spiral {
        return Spiral {
            x: 0,
            y: 0,
            coord_max: 0,
            x_rising: false,
            y_rising: false
        }
    }

    fn next(&mut self) -> Point {
        let point = Point {x: self.x, y: self.y };
        if self.x == -self.coord_max && self.y == -self.coord_max {
            self.x_rising = true;
            self.y_rising = true;
        }
        if self.x == self.coord_max && self.y == self.coord_max {
            self.x += 1;
            self.coord_max += 1;
            self.y_rising = false;
            self.x_rising = false;
        } else if !self.y_rising && self.y > -self.coord_max {
            self.y -= 1;
        } else if !self.x_rising && self.x > -self.coord_max {
            self.x -= 1;
        } else if self.y_rising && self.y < self.coord_max {
            self.y += 1;
        } else if self.x_rising && self.x < self.coord_max {
            self.x += 1;
        }
        return point;
    }
}

pub fn get_point_for_number(target_number: i32) -> Point {
    let mut spiral = Spiral::new();
    let mut point = Point {x: 0, y: 0};

    for _ in 1..(target_number + 1) {
        // store number
        point = spiral.next();
    }
    return point;
}

pub fn distance_from_point(point: Point) -> i32 {
    point.x.abs() + point.y.abs()
}

pub fn get_input(filename: &str) -> i32 {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let number: Vec<i32> = file.lines()
        .into_iter()
        .map(|line|line.unwrap())
        .map(|digit|digit.parse::<i32>().unwrap())
        .collect();
    return *number.first().unwrap();
}

pub fn solve_part_one(filename: &str) -> i32 {
    let point = get_point_for_number(get_input(filename));
    return distance_from_point(point);
}

pub fn get_value_below_number(target_number: i32) -> i32 {
    let mut spiral = Spiral::new();
    let mut map: HashMap<Point, i32> = HashMap::new();
    let mut value = 1;

    // set initial values
    let mut point = spiral.next();
    map.insert(point, value);

    while value < target_number {
        value = 0;
        point = spiral.next();
        for x in (point.x - 1)..(point.x + 2) {
            for y in (point.y - 1)..(point.y + 2) {
                let current_point = Point {x: x, y: y};
                match map.get(&current_point) {
                    Some(saved_value) => value += saved_value,
                    None => value += 0
                }
            }
        }
        map.insert(point, value);
    }
    return value;
}

pub fn solve_part_two(filename: &str) -> i32 {
    get_value_below_number(get_input(filename))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_1() {
        assert_eq!(distance_from_point(get_point_for_number(1)), 0);
    }
    
    #[test]
    fn example_2() {
        assert_eq!(distance_from_point(get_point_for_number(12)), 3);
    }
    
    #[test]
    fn example_3() {
        assert_eq!(distance_from_point(get_point_for_number(23)), 2);
    }

    #[test]
    fn example_4() {
        assert_eq!(distance_from_point(get_point_for_number(1024)), 31);
    }
    
    #[test]
    fn solve_part_1() {
        assert_eq!(solve_part_one("input.txt"), 438);
    }

    #[test]
    fn example_5() {
        assert_eq!(get_value_below_number(1), 1);
    }

    
    #[test]
    fn example_6() {
        assert_eq!(get_value_below_number(5), 5);
    }

    
    #[test]
    fn example_7() {
         assert_eq!(get_value_below_number(300), 304);
    }

    #[test]
    fn solve_part_2() {
        assert_eq!(solve_part_two("input.txt"), 266330);
    }
    
}
