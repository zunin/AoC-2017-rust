use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

pub fn get_input(filename: &str) -> Vec<String> {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let lines: Vec<String> = file.lines()
        .into_iter()
        .map(|line|line.unwrap())
        .collect();
    return lines;
}

pub fn check_passphrase(input: &str) -> bool {
    let mut result_map: HashMap<&str, i32> = HashMap::new();
    for word in input.split(" ").into_iter() {
        let counter = result_map.entry(word).or_insert(0);
        *counter += 1;
    }

    for (_word, count) in &result_map {
        if *count > 1 {
            return false;
        }
    }

    return true;
}

pub fn check_anagramable_passphrase(input: &str) -> bool {
    let mut result_map: HashMap<String, i32> = HashMap::new();
    for word in input.split(" ").into_iter() {
        let mut sorted_word: Vec<&str> = word.split("").collect();
        sorted_word.sort();
        let stringified_word = sorted_word.join("");

        let counter = result_map.entry(stringified_word).or_insert(0);
        *counter += 1;
    }

    for (_word, count) in &result_map {
        if *count > 1 {
            return false;
        }
    }

    return true;
}

pub fn solve_part_1(filename: &str) -> i32 {
    let lines = get_input(filename);
    let mut valid_inputs = 0;
    for line in lines {
        if check_passphrase(line.as_str()) {
            valid_inputs += 1;
        }
    }
    return valid_inputs;
}

pub fn solve_part_2(filename: &str) -> i32 {
    let lines = get_input(filename);
    let mut valid_inputs = 0;
    for line in lines {
        if check_anagramable_passphrase(line.as_str()) {
            valid_inputs += 1;
        }
    }
    return valid_inputs;
}

#[cfg(test)]
mod part_one {
    use super::*;

    #[test]
    fn example_1() {
        assert_eq!(check_passphrase("aa bb cc dd ee"), true);
    }
    
    #[test]
    fn example_2() {
        assert_eq!(check_passphrase("aa bb cc dd aa"), false);
    }
    
    #[test]
    fn example_3() {
        assert_eq!(check_passphrase("aa bb cc dd aaa"), true);
    }

    #[test]
    fn solve() {
        assert_eq!(solve_part_1("input.txt"), 337);
    }
}

#[cfg(test)]
mod part_two {
    use super::*;

    #[test]
    fn example_1() {
        assert_eq!(check_anagramable_passphrase("abcde fghij"), true);
    }

    #[test]
    fn example_2() {
        assert_eq!(check_anagramable_passphrase("abcde xyz ecdab"), false);
    }

    #[test]
    fn example_3() {
        assert_eq!(check_anagramable_passphrase("a ab abc abd abf abj"), true);
    }

    #[test]
    fn example_4() {
        assert_eq!(check_anagramable_passphrase("iiii oiii ooii oooi oooo"), true);
    }

    #[test]
    fn example_5() {
        assert_eq!(check_anagramable_passphrase("oiii ioii iioi iiio"), false);
    }

    #[test]
    fn solve() {
        assert_eq!(solve_part_2("input.txt"), 231);
    }
}