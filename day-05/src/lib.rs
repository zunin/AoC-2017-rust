use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

pub fn get_input(filename: &str) -> Vec<i32> {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let lines: Vec<i32> = file.lines()
        .into_iter()
        .map(|line|line.unwrap())
        .map(|digit| digit.parse::<i32>().unwrap())
        .collect();
    return lines;
}

pub fn count_jumps_until_exit(jump_offsets: Vec<i32>) -> i32 {
    let mut jumps = 0;
    let mut index = 0i32;

    let mut jump_list: Vec<i32> = jump_offsets.clone();

    loop {
        match jump_list.get_mut(index as usize) {
            Some(value) => {
                index += *value;
                *value += 1;
                jumps += 1;
            },
            None => break
        }
    }
    return jumps;
}

pub fn count_jumps_until_exit_max_3(jump_offsets: Vec<i32>) -> i32 {
    let mut jumps = 0;
    let mut index = 0i32;

    let mut jump_list: Vec<i32> = jump_offsets.clone();

    loop {
        match jump_list.get_mut(index as usize) {
            Some(value) => {
                index += *value;
                if *value >= 3 {
                    *value -= 1;
                } else {
                    *value += 1;
                }
                jumps += 1;
            },
            None => break
        }
    }
    return jumps;
}


#[cfg(test)]
mod part_one {
    use super::*;

    #[test]
    fn example_1() {
        assert_eq!(count_jumps_until_exit(vec![0, 3, 0, 1, -3]), 5);
    }

    #[test]
    fn solve() {
        assert_eq!(count_jumps_until_exit(get_input("input.txt")), 326618);
    }
}

#[cfg(test)]
mod part_two {
    use super::*;


    #[test]
    fn example_1() {
        assert_eq!(count_jumps_until_exit_max_3(vec![0, 3, 0, 1, -3]), 10);
    }

    #[test]
    fn solve() {
        assert_eq!(count_jumps_until_exit_max_3(get_input("input.txt")), 21841249);
    }
}