use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::collections::HashSet;

pub fn get_input(filename: &str) -> Vec<MemoryBank> {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let first_line: Vec<String> = file.lines()
        .into_iter()
        .map(|line| line.unwrap())
        .collect();

    let digits: Vec<&str> = first_line
        .first().unwrap()
        .split("\t")
        .collect();

    return digits
        .into_iter()
        .map(|digit| digit.parse::<usize>().unwrap())
        .map(|digit| MemoryBank {blocks: digit})
        .collect();
}

#[derive(Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub struct MemoryBank {
    blocks: usize 
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub struct State {
    memory_banks: Vec<MemoryBank>
}

pub fn distribute(memory_banks: &[MemoryBank]) -> Vec<MemoryBank> {
    let mut memory_banks_vec = memory_banks.clone().to_vec();
    let max_value = memory_banks.iter().max().unwrap();
    let max_index = memory_banks.iter().position(|x| x == max_value).unwrap();

    memory_banks_vec.remove(max_index);
    memory_banks_vec.insert(max_index, MemoryBank {blocks: 0});


    for index in (max_index + 1)..(max_index + max_value.blocks + 1) {
        let adjusted_index = index % memory_banks_vec.len();
        let current_bank = memory_banks_vec[adjusted_index].clone();
        memory_banks_vec.remove(adjusted_index);
        memory_banks_vec.insert(adjusted_index, MemoryBank {blocks: current_bank.blocks + 1});
    }
    
    return memory_banks_vec;
}

pub fn reallocate_memory(memory_banks: &[MemoryBank]) -> Vec<State> {
    let mut known_states: Vec<State> = Vec::new();
    
    let mut current_state = State {memory_banks: memory_banks.to_vec()};
    while !known_states.contains(&current_state) {
        known_states.push(current_state.clone());
        current_state = State {memory_banks: distribute(&current_state.memory_banks)};
    }
    return known_states;
}

pub fn find_cycle_size(memory_banks: &[MemoryBank]) -> usize {
    let states_before_repeat = reallocate_memory(memory_banks);

    let first_in_loop = State {memory_banks: distribute(&states_before_repeat.last().unwrap().memory_banks)};
    let index = states_before_repeat.iter().position(|x| x == &first_in_loop).unwrap();
    return states_before_repeat.len() - index;
}

#[cfg(test)]
mod mutation_test {
    use super::*;

    #[test]
    fn test_mutation_1() {
        assert!(
            distribute(&vec![
                MemoryBank {blocks: 0}, 
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 7}, 
                MemoryBank {blocks: 0}
            ]) ==
            vec![
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 4}, 
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 2}
            ]
        );
    }

    #[test]
    fn test_mutation_2() {
        assert!(
            distribute(&vec![
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 4}, 
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 2}
            ]) ==
            vec![
                MemoryBank {blocks: 3}, 
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 3}
            ]
        );
    }

    #[test]
    fn test_mutation_3() {
        assert!(
            distribute(&vec![
                MemoryBank {blocks: 3}, 
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 3}
            ]) ==
            vec![
                MemoryBank {blocks: 0}, 
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 3}, 
                MemoryBank {blocks: 4}
            ]
        );
    }

    #[test]
    fn test_mutation_4() {
        assert!(
            distribute(&vec![
                MemoryBank {blocks: 0}, 
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 3}, 
                MemoryBank {blocks: 4}
            ]) ==
            vec![
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 3}, 
                MemoryBank {blocks: 4}, 
                MemoryBank {blocks: 1}
            ]
        );
    }

    #[test]
    fn test_mutation_5() {
        assert!(
            distribute(&vec![
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 3}, 
                MemoryBank {blocks: 4}, 
                MemoryBank {blocks: 1}
            ]) ==
            vec![
                MemoryBank {blocks: 2}, 
                MemoryBank {blocks: 4}, 
                MemoryBank {blocks: 1}, 
                MemoryBank {blocks: 2}
            ]
        );
    }

}

#[cfg(test)]
mod part_one {
    use super::*;

    #[test]
    fn example() {
        assert_eq!(
            reallocate_memory(
                &vec![
                    MemoryBank {blocks: 0}, 
                    MemoryBank {blocks: 2}, 
                    MemoryBank {blocks: 7}, 
                    MemoryBank {blocks: 0}
                ]
            ).len(),
            5
        );
    }

    #[test]
    fn solve() {
        assert_eq!(reallocate_memory(&get_input("input.txt")).len(), 4074);
    }

}

#[cfg(test)]
mod part_two {
    use super::*;
    #[test]
    fn solve() {
        assert_eq!(find_cycle_size(&get_input("input.txt")), 2793);
    }
    //
}