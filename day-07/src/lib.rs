use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

pub fn get_input(filename: &str) -> Vec<Program> {
    let path = env::current_dir().unwrap().join("src").join(filename);
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);

    let lines: Vec<Program> = file.lines()
        .into_iter()
        .map(|line| line.unwrap())
        .map(|line| line_to_program(&line))
        .collect();
    return lines;
}

pub fn line_to_program(line_input: &str) -> Program {
    let mut line_split: Vec<&str> = line_input.split_whitespace().collect();
    line_split.reverse();
    let name = line_split.pop().unwrap().to_string();
    let weight = line_split.pop()
        .unwrap()
        .trim_matches(|c| c == '(' || c == ')')
        .parse::<usize>()
        .unwrap();

    line_split.pop();

    let programs_above: Vec<String> = line_split
        .iter()
        .map(|line| line.trim_matches(',').to_string())
        .collect();

    return Program { name: name, weight: weight, names_above: programs_above }; 
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub struct Program {
    name: String,
    weight: usize,
    names_above: Vec<String>
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub struct Node<'a> {
    program: &'a Program,
    parent: Option<&'a Program>,
    children: Option<Vec<&'a Program>>
}

pub fn get_children<'a>(program_list: &'a [Program], program: &'a Program) -> Option<Vec<&'a Program>> {
    let children = program_list
        .iter()
        .filter(|child_candidate| program.names_above.contains(&child_candidate.name))
        .collect::<Vec<&Program>>();


    if children.len() == 0 {
        return None;
    }

    return Some(children);
}

pub fn get_parent<'a>(program_list: &'a [Program], program: &'a Program) -> Option<&'a Program> {
    let parent_list = program_list
        .iter()
        .filter(|parent_candidate| parent_candidate.names_above.contains(&program.name))
        .collect::<Vec<&Program>>();
    
    if parent_list.len() == 0 {
        return None;
    }
    
    return Some(parent_list[0]);
    
}

pub fn create_tree(program_list: &[Program]) -> Vec<Node> {
    return program_list
        .iter()
        .map(|program| Node { program: program, parent: get_parent(program_list, program), children: get_children(program_list, program) })
        .collect::<Vec<Node>>();
}

pub fn find_root(program_list: &[Program]) -> Program {
    let tree = create_tree(program_list);
    //println!("{:?}", tree);
    return tree
        .iter()
        .map(|node| node.clone())
        .filter(|node| node.parent == None)
        .collect::<Vec<Node>>()
        .first()
        .unwrap().program.clone();
}


#[cfg(test)]
mod part_one {
    use super::*;

    #[test]
    fn example() {
        assert_eq!(find_root(&vec![
            Program { name: String::from("pbga"), weight: 66, names_above: vec![] },
            Program { name: String::from("xhth"), weight: 57, names_above: vec![] },
            Program { name: String::from("ebii"), weight: 61, names_above: vec![] },
            Program { name: String::from("havc"), weight: 66, names_above: vec![] },
            Program { name: String::from("ktlj"), weight: 57, names_above: vec![] },
            Program { name: String::from("fwft"), weight: 72, names_above: vec![String::from("ktlj"), String::from("cntj"), String::from("xhth")] },
            Program { name: String::from("qoyq"), weight: 66, names_above: vec![] },
            Program { name: String::from("padx"), weight: 45, names_above: vec![String::from("pbga"), String::from("havc"), String::from("qoyq")] },
            Program { name: String::from("tknk"), weight: 41, names_above: vec![String::from("ugml"), String::from("padx"), String::from("fwft")] },
            Program { name: String::from("jptl"), weight: 61, names_above: vec![] },
            Program { name: String::from("ugml"), weight: 68, names_above: vec![String::from("gyxo"), String::from("ebii"), String::from("jptl")] },
            Program { name: String::from("gyxo"), weight: 61, names_above: vec![] },
            Program { name: String::from("cntj"), weight: 57, names_above: vec![] },
        ]).name, "tknk".to_string());
    }

    #[test]
    fn solve() {
        assert_eq!(find_root(&get_input("input.txt")).name, String::from("eqgvf"));
    }

}

#[cfg(test)]
mod part_two {
    use super::*;
}